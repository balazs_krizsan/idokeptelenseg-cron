package com.idokep.Services;

import com.idokep.ValueObjects.DailyTemperature;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class IdokepParserService extends AbstractParserService implements ParserServiceInterface
{
    private final JsoupService jsoupService;
    private final String url;
    private final int siteId;

    public IdokepParserService(JsoupService jsoupService, String idokepUrl, int siteId)
    {
        this.jsoupService = jsoupService;
        this.url = idokepUrl;
        this.siteId = siteId;
    }

    public ArrayList<DailyTemperature> getWeatherForecastTemperatures() throws IOException, SQLException
    {
        Document document = jsoupService.getDocument(url);
        Elements temperatureColumnList = document.select("div.oszlop");
        ArrayList<DailyTemperature> dailyTemperatureList = new ArrayList<DailyTemperature>();
        Date queryDate = new Date();

        int i = 0;
        for (Element temperatureColumn : temperatureColumnList)
        {
            int minTemperature = getMinTemperature(temperatureColumn);
            int maxTemperature = getMaxTemperature(temperatureColumn);
            dailyTemperatureList.add(buildDailyTemperature(minTemperature, maxTemperature, queryDate, i, siteId));
            i++;
        }

        return dailyTemperatureList;
    }

    private int getMinTemperature(Element temperatureColumn)
    {
        return getTemperature(temperatureColumn, "min");
    }

    private int getMaxTemperature(Element temperatureColumn)
    {
        return getTemperature(temperatureColumn, "max");
    }

    private int getTemperature(Element temperatureColumn, String classPart)
    {
        String temperature = temperatureColumn
                .getElementsByAttributeValueContaining("class", classPart)
                .text()
                .replaceAll("[^\\d]*(\\d+).*", "$1");

        return Integer.parseInt(temperature);
    }
}
