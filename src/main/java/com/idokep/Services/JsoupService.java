package com.idokep.Services;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;

public class JsoupService
{
    public Document getDocument(String url) throws IOException
    {
        return Jsoup.connect(url).get();
    }

    public Document postDocument(String url, Map<String, String> postData) throws IOException
    {
        Connection connection = Jsoup.connect(url);
        for (Map.Entry<String, String> item : postData.entrySet())
        {
            connection.data(item.getKey(), item.getValue());
        }

        return connection.userAgent("Mozilla").post();
    }
}
