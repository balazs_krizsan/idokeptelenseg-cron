package com.idokep.Services;

import com.idokep.ValueObjects.DailyTemperature;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ParserServiceInterface
{
    public ArrayList<DailyTemperature> getWeatherForecastTemperatures() throws IOException, SQLException;
}
