package com.idokep.Services;

import com.idokep.ValueObjects.DailyTemperature;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

abstract class AbstractParserService
{
    protected Date getTemperatureDateByDay(Integer day)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    protected DailyTemperature buildDailyTemperature(
            int minTemperature,
            int maxTemperature,
            Date queryDate,
            int i,
            int siteId
    )
    {
        return new DailyTemperature(
                queryDate,
                getTemperatureDateByDay(i),
                minTemperature,
                maxTemperature,
                siteId
        );
    }
}
