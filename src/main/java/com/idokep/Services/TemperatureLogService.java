package com.idokep.Services;

import com.idokep.Repositories.TemperatureLogRepository;
import com.idokep.ValueObjects.DailyTemperature;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TemperatureLogService
{
    private TemperatureLogRepository temperatureLogRepository;
    private final Connection connection;

    public TemperatureLogService(TemperatureLogRepository temperatureLogRepository, Connection connection)
    {
        this.temperatureLogRepository = temperatureLogRepository;
        this.connection = connection;
    }

    public void create(ArrayList<DailyTemperature> dailyTemperatureList) throws SQLException
    {
        connection.setAutoCommit(false);

        try
        {
            for (DailyTemperature dailyTemperatureItem : dailyTemperatureList)
            {
                temperatureLogRepository.create(dailyTemperatureItem);
                consoleLog(dailyTemperatureItem);
            }

            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        }
    }

    //@todo: remove it later
    private void consoleLog(DailyTemperature dailyTemperatureItem)
    {
        DateFormat dateFormat = new SimpleDateFormat("Y-M-d");

        System.out.println("Current date: " + dateFormat.format(dailyTemperatureItem.getQueryDate()));
        System.out.println("Temperature date: " + dateFormat.format(dailyTemperatureItem.getTemperatureDate()));
        System.out.println("min: " + dailyTemperatureItem.getMinTemperature());
        System.out.println("max: " + dailyTemperatureItem.getMaxTemperature());
        System.out.println("siteId: " + dailyTemperatureItem.getSiteId());
        System.out.println("");
    }
}
