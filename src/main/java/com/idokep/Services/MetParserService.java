package com.idokep.Services;

import com.idokep.ValueObjects.DailyTemperature;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MetParserService extends AbstractParserService implements ParserServiceInterface
{
    private final JsoupService jsoupService;
    private final String url;
    private final int siteId;

    public MetParserService(JsoupService jsoupService, String idokepUrl, int siteId)
    {
        this.jsoupService = jsoupService;
        this.url = idokepUrl;
        this.siteId = siteId;
    }

    public ArrayList<DailyTemperature> getWeatherForecastTemperatures() throws IOException, SQLException
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("valtozatlan", "first");
        params.put("kod", "1");
        params.put("lt", "47.50");
        params.put("n", "19.10");
        params.put("tel", "Budapest");
        Document document = jsoupService.postDocument(url, params);

        Date queryDate = new Date();

        ArrayList<DailyTemperature> dailyTemperatureList = new ArrayList<DailyTemperature>();
        ArrayList<Integer> minTemperatureList = getMinTemperatureList(document);
        ArrayList<Integer> maxTemperatureList = getMaxTemperatureList(document);

        int i = 0;
        for (int minTemperature : minTemperatureList)
        {
            int maxTemperature = maxTemperatureList.remove(0);
            dailyTemperatureList.add(buildDailyTemperature(minTemperature, maxTemperature, queryDate, i, siteId));
            i++;
        }

        return dailyTemperatureList;
    }

    private ArrayList<Integer> getMaxTemperatureList(Document document)
    {
        return getTemperatureList(document, ".meteogram tr.hmax div");
    }

    private ArrayList<Integer> getMinTemperatureList(Document document)
    {
        return getTemperatureList(document, ".meteogram tr.hmin div");
    }

    private ArrayList<Integer> getTemperatureList(Document document, String selector)
    {
        Elements maxTemperatureDivList = document.select(selector);
        ArrayList<Integer> maxTemperatureList = new ArrayList<Integer>();

        boolean isFirstTd = true;
        for (Element temperatureColumn : maxTemperatureDivList)
        {
            if (isFirstTd)
            {
                isFirstTd = false;

                continue;
            }
            String result = temperatureColumn.text().replace(".*(\\d+).*", "$1");
            maxTemperatureList.add(Integer.parseInt(result));
        }

        return maxTemperatureList;
    }
}
