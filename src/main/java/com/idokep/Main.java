package com.idokep;

import com.idokep.Repositories.TemperatureLogRepository;
import com.idokep.Services.*;
import com.idokep.ValueObjects.DailyTemperature;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main
{
    private final static String psqlConnString = "jdbc:postgresql://localhost/idokep?user=postgres&password=lsg815";
    private final static String idokepUrl = "https://www.idokep.hu/30napos";
    private final static int idokepSiteId = 1;
    private final static String koponyegUrl = "http://koponyeg.hu/t/Budapest";
    private final static int koponyegSiteId = 2;
    private final static String metUrl = "http://www.met.hu/idojaras/elorejelzes/magyarorszagi_telepulesek/main.php";
    private final static int metSiteId = 3;

    public static void main(String[] args)
    {
        Connection connection = null;
        try
        {
            connection = DriverManager.getConnection(psqlConnString);

            TemperatureLogRepository temperatureLogRepository = new TemperatureLogRepository(connection);

            IdokepParserService idokepParser = new IdokepParserService(new JsoupService(), idokepUrl, idokepSiteId);
            KoponyegParserService koponyegParser = new KoponyegParserService(
                    new JsoupService(),
                    koponyegUrl,
                    koponyegSiteId
            );
            MetParserService metParser = new MetParserService(
                    new JsoupService(),
                    metUrl,
                    metSiteId
            );
            TemperatureLogService temperatureLogService = new TemperatureLogService(
                    temperatureLogRepository,
                    connection
            );

            ArrayList<DailyTemperature> temperatureList = new ArrayList<DailyTemperature>();

            temperatureList.addAll(idokepParser.getWeatherForecastTemperatures());
            temperatureList.addAll(koponyegParser.getWeatherForecastTemperatures());
            temperatureList.addAll(metParser.getWeatherForecastTemperatures());

            temperatureLogService.create(temperatureList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (null != connection)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
