package com.idokep.Repositories;

import com.idokep.ValueObjects.DailyTemperature;

import java.sql.*;

public class TemperatureLogRepository
{
    private Connection connection;

    public TemperatureLogRepository(Connection connection)
    {
        this.connection = connection;
    }

    public void create(DailyTemperature dailyTemperature) throws SQLException
    {
        String sql = "INSERT INTO " +
                "temperature_log (query_date, temperature_date, min_temperature, max_temperature, site_id)" +
                "VALUES (?, ?, ?, ?, ?)";

        PreparedStatement insertStmt = this.connection.prepareStatement(sql);
        insertStmt.setDate(1, dailyTemperature.getQuerySqlDate());
        insertStmt.setDate(2, dailyTemperature.getTemperatureSqlDate());
        insertStmt.setInt(3, dailyTemperature.getMinTemperature());
        insertStmt.setInt(4, dailyTemperature.getMaxTemperature());
        insertStmt.setInt(5, dailyTemperature.getSiteId());
        insertStmt.execute();
    }
}
