package com.idokep.ValueObjects;

import java.util.Date;

public class DailyTemperature
{
    private final Date queryDate;
    private final Date temperatureDate;
    private final int minTemperature;
    private final int maxTemperature;
    private final int siteId;

    public DailyTemperature(
            Date queryDate,
            Date temperatureDate,
            int minTemperature,
            int maxTemperature,
            int siteId
    )
    {
        this.queryDate = queryDate;
        this.temperatureDate = temperatureDate;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
        this.siteId = siteId;
    }

    public Date getQueryDate()
    {
        return queryDate;
    }

    public java.sql.Date getQuerySqlDate()
    {
        return new java.sql.Date(getQueryDate().getTime());
    }

    public Date getTemperatureDate()
    {
        return temperatureDate;
    }

    public java.sql.Date getTemperatureSqlDate()
    {
        return new java.sql.Date(getTemperatureDate().getTime());
    }

    public int getMinTemperature()
    {
        return minTemperature;
    }

    public int getMaxTemperature()
    {
        return maxTemperature;
    }

    public int getSiteId()
    {
        return siteId;
    }
}
