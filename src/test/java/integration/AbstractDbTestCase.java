package integration;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class AbstractDbTestCase extends TestCase
{
    private final static String psqlConnString = "jdbc:postgresql://localhost/idokeptest?user=postgres&password=lsg815";
    private Connection connection;

    @Before
    public final void setUp() throws Exception
    {
        super.setUp();
        connection = DriverManager.getConnection(psqlConnString);
        connection.setAutoCommit(false);
    }

    protected Connection getTestConnection()
    {
        return connection;
    }

    @After
    public final void tearDown() throws Exception
    {
        super.tearDown();
        connection.rollback();
        connection.close();
    }
}
