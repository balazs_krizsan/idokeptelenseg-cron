package integration.repositories;

import com.idokep.Repositories.TemperatureLogRepository;
import com.idokep.ValueObjects.DailyTemperature;
import integration.AbstractDbTestCase;
import org.junit.Test;

import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TemperaturesRepositoryTest extends AbstractDbTestCase
{
    @Test
    public void testQwe() throws SQLException
    {
        // Arrange
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, 2011);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 11);
        java.sql.Date querySqlDate = new java.sql.Date(calendar.getTime().getTime());

        calendar.set(Calendar.YEAR, 2022);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 22);
        java.sql.Date temperatureSqlDate = new java.sql.Date(calendar.getTime().getTime());

        int minTemperature = 1;
        int maxTemperature = 2;

        DailyTemperature dailyTemperature = new DailyTemperature(
                querySqlDate,
                temperatureSqlDate,
                minTemperature,
                maxTemperature,
                1
        );

        String expectedQueryDate = "2011-01-11";
        String expectedTemperatureDate = "2022-02-22";
        String expectedMinTemperature = "1";
        String expectedMaxTemperature = "2";

        // Act
        TemperatureLogRepository temperaturesRepository = new TemperatureLogRepository(getTestConnection());
        temperaturesRepository.create(dailyTemperature);

        // Assert
        String query = "SELECT * FROM temperature_log " +
                "WHERE query_date=? AND temperature_date=? AND min_temperature=? AND max_temperature=?;";

        PreparedStatement select = getTestConnection().prepareStatement(query);
        select.setDate(1, dailyTemperature.getQuerySqlDate());
        select.setDate(2, dailyTemperature.getTemperatureSqlDate());
        select.setInt(3, dailyTemperature.getMinTemperature());
        select.setInt(4, dailyTemperature.getMaxTemperature());

        ResultSet rs = select.executeQuery();
        rs.next();
        assertEquals(expectedQueryDate, rs.getString("query_date"));
        assertEquals(expectedTemperatureDate, rs.getString("temperature_date"));
        assertEquals(expectedMinTemperature, rs.getString("min_temperature"));
        assertEquals(expectedMaxTemperature, rs.getString("max_temperature"));
    }
}
