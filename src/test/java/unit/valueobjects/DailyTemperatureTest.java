package unit.valueobjects;

import com.idokep.ValueObjects.DailyTemperature;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DailyTemperatureTest
{
    @Test
    public void value_object_test_X_perfect()
    {
        // Arrange
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2011);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date queryDate = calendar.getTime();
        java.sql.Date querySqlDate = new java.sql.Date(queryDate.getTime());

        calendar.set(Calendar.YEAR, 2022);
        calendar.set(Calendar.MONTH, 2);
        calendar.set(Calendar.DAY_OF_YEAR, 2);
        Date temperatureDate = calendar.getTime();
        java.sql.Date temperatureSqlDate = new java.sql.Date(temperatureDate.getTime());

        int dailyMinTemperature = 1;
        int dailyMaxTemperature = 2;

        // Act
        DailyTemperature dailyTemperature = new DailyTemperature(
                queryDate,
                temperatureDate,
                dailyMinTemperature,
                dailyMaxTemperature,
                1
        );

        // Assert
        assertEquals(dailyMinTemperature, dailyTemperature.getMinTemperature());
        assertEquals(dailyMaxTemperature, dailyTemperature.getMaxTemperature());
        assertEquals(queryDate, dailyTemperature.getQueryDate());
        assertEquals(querySqlDate, dailyTemperature.getQuerySqlDate());
        assertEquals(temperatureDate, dailyTemperature.getTemperatureDate());
        assertEquals(temperatureSqlDate, dailyTemperature.getTemperatureSqlDate());
    }
}
